package com.gurdeep.walletmanager

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.gurdeep.walletmanager.data.TransactionModelProviderFactory
import com.gurdeep.walletmanager.databinding.ActivityMainBinding
import com.gurdeep.walletmanager.ui.FragmentTransactionDirections

class MainActivity : AppCompatActivity() {

    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val navigationFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment)  as NavHostFragment
        navController = navigationFragment.navController


    }

    public fun navigate(){
        try {
            val action =
                FragmentTransactionDirections.actionFragmentTransactionToFragmentAddTransaction()
            navController.navigate(action)
        }
        catch (e: Exception){
            e.printStackTrace()
        }
    }
}
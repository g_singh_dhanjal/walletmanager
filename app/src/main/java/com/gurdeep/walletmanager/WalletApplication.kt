package com.gurdeep.walletmanager

import android.app.Application
import com.gurdeep.walletmanager.data.repository.CategoryRepository
import com.gurdeep.walletmanager.data.repository.TransactionRepository
import com.gurdeep.walletmanager.data.database.WalletDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class WalletApplication : Application() {
    val applicationScope = CoroutineScope(SupervisorJob())

    val database by lazy { WalletDatabase.getDbInstance(this,applicationScope) }
    val transactionRepository by lazy { TransactionRepository(database.transactionDao()) }
    val categoryRepository by lazy { CategoryRepository(database.categoryDao()) }


}
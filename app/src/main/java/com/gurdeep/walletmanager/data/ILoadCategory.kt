package com.gurdeep.walletmanager.data

import com.gurdeep.walletmanager.data.model.Category
import java.util.*

interface ILoadCategory {

    fun loadCategoryData(): List<Category>
}
package com.gurdeep.walletmanager.data.database.dao

import androidx.room.*
import com.gurdeep.walletmanager.data.model.Transaction
import kotlinx.coroutines.flow.Flow
import kotlin.collections.List

@Dao
interface TransactionDao {

    @Query("SELECT * FROM `transaction` ORDER BY tid DESC")
      fun getAllTransactions(): Flow<List<Transaction>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
   suspend fun insert(transaction: Transaction)

    @Update
    suspend fun update(transaction: Transaction)

    @Query("DELETE FROM `transaction`")
    suspend fun remove()
}
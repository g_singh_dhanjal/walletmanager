package com.gurdeep.walletmanager.data.repository

import androidx.annotation.WorkerThread
import com.gurdeep.walletmanager.data.database.dao.TransactionDao
import com.gurdeep.walletmanager.data.model.Transaction

class TransactionRepository(private val transactionDao: TransactionDao ) {

    fun getAllTransactions() = transactionDao.getAllTransactions()

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(transaction: Transaction) {
        transactionDao.insert(transaction)
    }
    @WorkerThread
    suspend fun update(transaction: Transaction) {
        transactionDao.update(transaction)
    }
}
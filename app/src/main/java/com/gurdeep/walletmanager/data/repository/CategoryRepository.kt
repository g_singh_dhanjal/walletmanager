package com.gurdeep.walletmanager.data.repository

import androidx.annotation.WorkerThread
import com.gurdeep.walletmanager.data.database.dao.CategoryDao
import com.gurdeep.walletmanager.data.model.Category


class CategoryRepository(private val categoryDao: CategoryDao) {

    fun getAllCategories() = categoryDao.getAllCategories()


    @WorkerThread
    suspend fun insert(category: Category) {
        categoryDao.insert(category)
    }
    @WorkerThread
    suspend fun update(category: Category) {
        categoryDao.update(category)
    }
}
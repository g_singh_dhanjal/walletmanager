package com.gurdeep.walletmanager.data

import com.gurdeep.walletmanager.data.model.Category

class LoadStaticCategories : ILoadCategory{
    override fun loadCategoryData(): List<Category> {
        val categoryRent  =  Category(categoryName =  "Rent",maxBudget = 1600)
        val internetBill  = Category(categoryName =  "Internet bill",maxBudget = 70)
        val powerBill  = Category(categoryName =  "Power bill ",maxBudget = 120)
        val insurance  = Category(categoryName =  "Insurance ",maxBudget = 70)
        val transport  = Category(categoryName =  "Transport ",maxBudget = 140)
        val grocery   = Category(categoryName =  "Grocery ",maxBudget = 300)

      return listOf<Category>(categoryRent,internetBill,powerBill,insurance,transport,grocery)
    }


}
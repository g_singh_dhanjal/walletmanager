package com.gurdeep.walletmanager.data.util

import java.text.SimpleDateFormat
import java.util.*

class Utility {

    companion object {

        fun convertTimeToString() {}
        fun getMillisFromCalender(year: Int, month: Int, day: Int): Long {
            val calendar: Calendar = Calendar.getInstance()
            calendar.set(year, month, day)
            return calendar.timeInMillis
        }

        fun formatDateToMMddyyyy(longMilliseconds: Long): String {
            val format = SimpleDateFormat("MM-dd-yyyy")
            val strDate: String = format.format(longMilliseconds)
            return strDate
        }
        fun formatDateToHHmm(longMilliseconds: Long): String {
            val format = SimpleDateFormat("hh:mm")
            val strDate: String = format.format(longMilliseconds)
            return strDate
        }
    }

}
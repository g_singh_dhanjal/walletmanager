package com.gurdeep.walletmanager.data.database.dao

import androidx.room.*
import com.gurdeep.walletmanager.data.model.Category
import com.gurdeep.walletmanager.data.model.Transaction
import kotlinx.coroutines.flow.Flow

@Dao
interface CategoryDao {

    @Query("SELECT * FROM `category` ORDER BY cid ASC")
    fun getAllCategories(): Flow<List<Category>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(category: Category)

    @Update
    suspend fun update(category: Category)

    @Query("DELETE FROM `category`")
    suspend fun remove()
}
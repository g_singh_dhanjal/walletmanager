package com.gurdeep.walletmanager.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.gurdeep.walletmanager.data.ILoadCategory
import com.gurdeep.walletmanager.data.LoadStaticCategories
import com.gurdeep.walletmanager.data.database.dao.CategoryDao
import com.gurdeep.walletmanager.data.database.dao.TransactionDao
import com.gurdeep.walletmanager.data.model.Category
import com.gurdeep.walletmanager.data.model.Transaction
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = arrayOf(Transaction::class, Category::class), version = 1)
abstract class WalletDatabase : RoomDatabase() {
    abstract fun transactionDao(): TransactionDao
    abstract fun categoryDao(): CategoryDao

    companion object {
        @Volatile
        private var INSTANCE: WalletDatabase? = null

        fun getDbInstance(
            applicationContext: Context, scope: CoroutineScope
        ): WalletDatabase {

            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    applicationContext,
                    WalletDatabase::class.java, "wallet_database"
                ).addCallback(AddCategoryDatabaseCallback(scope = scope, LoadStaticCategories()))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
    class AddCategoryDatabaseCallback(
        private val scope: CoroutineScope,
        private val loadCategory: ILoadCategory
    ) : Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            INSTANCE?.let { database ->
                scope.launch {
                    populateCategoryTable((database ).categoryDao() )
                }
            }

            super.onCreate(db)
        }

        suspend fun populateCategoryTable (categoryDao: CategoryDao) {
            // Delete all content here.
            categoryDao.remove()
            val categoryList = loadCategory.loadCategoryData();
            for (category in categoryList) {
                categoryDao.insert(category)
            }

        }

    }
}



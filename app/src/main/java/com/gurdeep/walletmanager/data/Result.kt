package com.gurdeep.walletmanager.data

import androidx.lifecycle.LiveData
import java.lang.Exception

sealed class Result {
//data class  Success(val  data: )
data class  Error(val exception: Exception)
}
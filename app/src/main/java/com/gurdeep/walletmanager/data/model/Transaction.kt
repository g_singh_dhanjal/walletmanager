package com.gurdeep.walletmanager.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Transaction(@PrimaryKey(autoGenerate = true) var tid: Int=0,
                       @ColumnInfo(name = "cid") var cid: Int=0,
                       @ColumnInfo(name = "cname") var cname: String?="",
                       @ColumnInfo(name = "value") var value: String?="",
                       @ColumnInfo(name = "currency") var currency: String?="",
                       @ColumnInfo(name = "date") var date : Long=0,
                       @ColumnInfo(name = "time") var time : String?=""

) {
}
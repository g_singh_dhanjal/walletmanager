package com.gurdeep.walletmanager.data

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gurdeep.walletmanager.WalletApplication
import com.gurdeep.walletmanager.ui.CategoryViewModel
import com.gurdeep.walletmanager.ui.SharedViewModel
import com.gurdeep.walletmanager.ui.TransactionViewModel

class TransactionModelProviderFactory(private val applicationContext: Context?): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TransactionViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return TransactionViewModel((applicationContext as WalletApplication).transactionRepository ) as T
        }
        else if (modelClass.isAssignableFrom(CategoryViewModel::class.java)) {

            return CategoryViewModel((applicationContext as WalletApplication).categoryRepository) as T
        }
        else if (modelClass.isAssignableFrom(SharedViewModel::class.java)) {

            return SharedViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
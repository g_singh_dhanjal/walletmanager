package com.gurdeep.walletmanager.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Category(
    @PrimaryKey(autoGenerate = true) var cid: Int=0,
    @ColumnInfo(name = "categoryName") var categoryName: String?,
    @ColumnInfo(name = "maxBudget")var maxBudget : Int
) {
}
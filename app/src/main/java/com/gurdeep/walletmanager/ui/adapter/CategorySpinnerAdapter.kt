package com.gurdeep.walletmanager.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.gurdeep.walletmanager.R
import com.gurdeep.walletmanager.data.model.Category
import com.gurdeep.walletmanager.databinding.CustomSpinnerLayoutBinding


class CategorySpinnerAdapter(ctx: Context,
                             moods: List<Category>) :
    ArrayAdapter<Category>(ctx, 0, moods) {

    override fun getView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    override fun getDropDownView(position: Int, recycledView: View?, parent: ViewGroup): View {
        return this.createView(position, recycledView, parent)
    }

    private fun createView(position: Int, recycledView: View?, parent: ViewGroup): View {

        val category = getItem(position)
        val view =  CustomSpinnerLayoutBinding.inflate(LayoutInflater.from(parent.context), null, false)

        if (category != null) {
            bind(view,category)
        }
        return view.root
    }

    fun bind(binding: CustomSpinnerLayoutBinding,category : Category){
        binding.txtCategoryName.text = category.categoryName

    }
}
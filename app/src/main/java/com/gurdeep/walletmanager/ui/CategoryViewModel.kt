package com.gurdeep.walletmanager.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.gurdeep.walletmanager.data.repository.CategoryRepository
import com.gurdeep.walletmanager.data.model.Category
import kotlinx.coroutines.launch

class CategoryViewModel (private val categoryRepository: CategoryRepository) : ViewModel() {
    val allCategories : LiveData<List<Category>> = categoryRepository.getAllCategories().asLiveData()


    fun insert(category: Category) = viewModelScope.launch {
        categoryRepository.insert(category)
    }
}
package com.gurdeep.walletmanager.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gurdeep.walletmanager.data.model.Transaction

class SharedViewModel : ViewModel() {

    var liveDataTransaction = MutableLiveData<Transaction>()
    fun sendTransaction(transaction: Transaction){
        liveDataTransaction.value = transaction
    }
}
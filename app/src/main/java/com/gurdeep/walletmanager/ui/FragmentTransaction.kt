package com.gurdeep.walletmanager.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.gurdeep.walletmanager.MainActivity
import com.gurdeep.walletmanager.R
import com.gurdeep.walletmanager.data.TransactionModelProviderFactory
import com.gurdeep.walletmanager.databinding.FragmentTransactionBinding
import com.gurdeep.walletmanager.ui.adapter.TransactionAdapter

class FragmentTransaction : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentTransactionBinding.inflate(inflater,null,false)
        val recyclerView = binding.recyclerviewTransaction
        recyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.addItemDecoration(DividerItemDecoration(activity,DividerItemDecoration.VERTICAL))

        val transactionViewModel by viewModels<TransactionViewModel>(){
            TransactionModelProviderFactory(activity?.applicationContext)

        }

        transactionViewModel.allTransactions.observe(this, Observer {
          it?:return@Observer
          recyclerView.adapter = TransactionAdapter(it)

        })
        binding.floatingActionButton.setOnClickListener {
            (activity as MainActivity). navigate()

        }
        return binding.root
    }


}
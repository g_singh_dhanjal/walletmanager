package com.gurdeep.walletmanager.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.gurdeep.walletmanager.data.repository.TransactionRepository
import com.gurdeep.walletmanager.data.model.Transaction
import kotlinx.coroutines.launch


class TransactionViewModel(private val transactionRepository: TransactionRepository) : ViewModel() {
       val allTransactions : LiveData<List<Transaction>> = transactionRepository.getAllTransactions().asLiveData()


     fun insert(transaction: Transaction) = viewModelScope.launch {
        transactionRepository.insert(transaction)
    }

}
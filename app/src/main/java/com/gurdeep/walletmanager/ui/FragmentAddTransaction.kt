package com.gurdeep.walletmanager.ui

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.gurdeep.walletmanager.R
import com.gurdeep.walletmanager.data.TransactionModelProviderFactory
import com.gurdeep.walletmanager.data.model.Category
import com.gurdeep.walletmanager.data.model.Transaction
import com.gurdeep.walletmanager.data.util.Utility
import com.gurdeep.walletmanager.databinding.FragmentAddTransactionBinding
import com.gurdeep.walletmanager.ui.adapter.CategorySpinnerAdapter
import java.lang.Exception
import java.util.*

class FragmentAddTransaction : Fragment(), View.OnClickListener,
    AdapterView.OnItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private lateinit var binding: FragmentAddTransactionBinding
    private lateinit var categoryList: List<Category>
    private val transactionViewModel by viewModels<TransactionViewModel>(){
        TransactionModelProviderFactory(activity?.applicationContext)

    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddTransactionBinding.inflate(inflater, null, false)

        val categoryViewModel by viewModels<CategoryViewModel>() {
            TransactionModelProviderFactory(activity?.applicationContext)

        }

        categoryViewModel.allCategories.observe(this, Observer {
            it ?: return@Observer
            categoryList = it
            context?.let { context ->
                populateSpinnerDataForCategory(context, it)
                populateSpinnerDataForCurrency(context, R.array.array_currency)
            }
        })

        initListeners()
        return binding.root
    }

    private fun initListeners() {
        binding.btnAdd.setOnClickListener(this)
        binding.btnTime.setOnClickListener(this)
        binding.btnDate.setOnClickListener(this)
        binding.spinnerCategory.onItemSelectedListener = this
        binding.spinnerCurrency.onItemSelectedListener = this
        addTextWatcher()


    }
    private fun addTextWatcher(){
        binding.txtInputExpenseValue.editText?.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                try {

                    transaction.value = p0.toString()
                    binding.txtInputExpenseValue.isErrorEnabled = false
                }
                catch (e:Exception){}
            }
            override fun afterTextChanged(p0: Editable?) {

            }
        })
    }

    val transaction = Transaction()
    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnTime -> {
                showTimePicker()
            }
            R.id.btnDate -> {
                showDatePicker()
            }
            R.id.btnAdd -> {
                sendTransaction()
            }
        }
    }

    private fun sendTransaction() {
        if (binding.txtInputExpenseValue.editText?.text.toString().isNotEmpty()) {
            transactionViewModel.insert(transaction)
            Toast.makeText(context, R.string.transaction_success, Toast.LENGTH_LONG).show()
        }
        else {
            binding.txtInputExpenseValue.error = context?.getString(R.string.enter_valid_expense)

        }
    }

    private fun showDatePicker() {
        val cal = Calendar.getInstance()
        val y = cal.get(Calendar.YEAR)
        val m = cal.get(Calendar.MONTH)
        val d = cal.get(Calendar.DAY_OF_MONTH)
        var date = System.currentTimeMillis()
        context?.let { context ->
            val datePickerDialog: DatePickerDialog = DatePickerDialog(
                context,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                     date = Utility.getMillisFromCalender(year, monthOfYear, dayOfMonth)
                    Toast.makeText(context, "date ${date}", Toast.LENGTH_LONG).show()
                    transaction.date = date
                },
                y,
                m,
                d
            )

            datePickerDialog.show()
        }
    }

    private fun showTimePicker() {
        val c: Calendar = Calendar.getInstance()
        val hh = c.get(Calendar.HOUR_OF_DAY)
        val mm = c.get(Calendar.MINUTE)
        context?.let { context ->
            val timePickerDialog: TimePickerDialog =
                TimePickerDialog(
                    context,
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        val time = "${hourOfDay}-${minute}"
                        transaction.time = time
                    },
                    hh,
                    mm,
                    true
                )
            timePickerDialog.show()
        }

    }

    private fun populateSpinnerDataForCategory(context: Context, category: List<Category>) {
        binding.spinnerCategory.adapter = CategorySpinnerAdapter(context, category)
    }

    private fun populateSpinnerDataForCurrency(context: Context, resId: Int) {
        ArrayAdapter.createFromResource(
            context,
            resId,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.spinnerCurrency.adapter = adapter
        }
    }

    override fun onItemSelected(
        adapterView: AdapterView<*>?,
        view: View?,
        p2: Int,
        position: Long
    ) {
        if (adapterView?.id == R.id.spinner_category) {
            transaction.cid = categoryList?.get(position.toInt()).cid
            transaction.cname = categoryList?.get(position.toInt()).categoryName
        } else if (adapterView?.id == R.id.spinner_currency) {

            var currency = "NZD"
            if (position.toInt() ==1){
                currency = "USD"
            }
            transaction.currency = currency
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {

    }


}
package com.gurdeep.walletmanager.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gurdeep.walletmanager.data.model.Transaction
import com.gurdeep.walletmanager.databinding.ListItemTransactionBinding
import com.gurdeep.walletmanager.ui.adapter.TransactionAdapter.TransactionViewHolder

class TransactionAdapter(private val transactionList: List<Transaction>) :
    RecyclerView.Adapter<TransactionViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {

        val binding =
            ListItemTransactionBinding.inflate(LayoutInflater.from(parent.context), null, false)
        return TransactionViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bind(transactionList[position])
    }

    class TransactionViewHolder(private val binding: ListItemTransactionBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(transaction: Transaction) {
            transaction.value  = transaction.value+ " "+ transaction.currency
            binding.transaction = transaction
        }
    }
    override fun getItemCount() = transactionList?.size


}